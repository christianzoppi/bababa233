if(!flash.initialised) {
	// Custom Events
	var flashReadyEvent = new CustomEvent('flashReady');

	Turbolinks.start();
	TurbolinksHover.start();
	flash.initialised = true;
	flash.start();

    // This var will store the clean html when you land on a page to
    // restore it before caching
    var docbody = '';
    var body_classes = '';

	document.addEventListener("turbolinks:load", function(){
        docbody = document.querySelector('body').innerHTML;
        body_classes = document.querySelector('body').className;
        flash.start();
    });
    
    document.addEventListener("turbolinks:before-cache", function() {
        document.querySelector('body').innerHTML = docbody;
        document.querySelector('body').className = body_classes;
        document.querySelector('body').setAttribute('style','');
    });
}