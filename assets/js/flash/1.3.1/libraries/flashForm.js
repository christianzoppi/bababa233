/**
 * v 1.0
 * Flash Forms Management
 * ---------------------------
 * 
 *
 * Add .js_form--no_flash to a form to make sure it's ignored by this script
 * or add .js_form--just_in_lightbox to make it work just in a lightbox
 */

var flash_current_form = null;

// Form Submission
function flashForm(element) {
    var self = this;
    // If an element is passed, the form is initialised
    if(element) {
        self.form = element;
        if(!self.form.classList.contains('js_init--flash-form')) {
            self.getSettings();
            self.initialise();
            self.fieldsEvents();
        }
    }
}

// Submissions handler url
flashForm.prototype.flash_submissions_handler = 'https://463z3kbl0f.execute-api.eu-west-1.amazonaws.com/default/flash-forms-handler-multipart';

// Check if submissions are enabled
flashForm.prototype.isEnabled = function() {
    var self = this;

    return self.settings.enabled;
}

// For initialisation
flashForm.prototype.initialise = function(){
    var self = this;

    // Add init class
    self.form.classList.add('js_init--flash-form');

    // JUST AJAX SUBMISSIONS
    if(self.settings.ajax_submit) {
        self.form.addEventListener('submit', function(event){
            // Disable Submit button
            var submit_button = self.form.querySelector('[type="submit"]');
            if(submit_button) {
                submit_button.disabled = true;
            }

            event.preventDefault();
            flash_current_form = self;
            self.preSubmissionActions();

            // Check if the form is enabled
            if(!self.isEnabled()) {
                self.afterSubmissionActions('error');
                return;
            }
            
            // Submission promises
            var submissions = [];

            // If the form has the cnf file
            // the script will submit also to lambda
            if(self.form.querySelectorAll('[name="cnf"]').length) {
                submissions.push(self.flashHandler());
            }
    
            // If the form has the action attribute
            // the script will submit also the the custom post url
            if(self.form.hasAttribute('action')) {
                submissions.push(self.otherHandler());
            }
            
            // Waiting for the requests to be completed
            Promise.all(submissions)
            .then(function ( data ) {
                self.afterSubmissionActions('success');
            })
            .catch(function(error){
                self.afterSubmissionActions('error');
            });
        });
    } else if(self.form.hasAttribute('action') && self.form.getAttribute('action') != '') {
        // NOT JUST AJAX SUBMISSIONS   
        // Add this class to prevent automatic posting to the action URL
        self.form.classList.add('js-prevent-submit');
        // Handling submission
        self.form.addEventListener('submit', function(event){
            flash_current_form = self;
            if(self.form.classList.contains('js-prevent-submit')) {
                self.preSubmissionActions();
                event.preventDefault();
                // Submission promises
                var submissions = [];

                // If the form has the cnf file
                // the script will submit also to lambda
                if(self.form.querySelectorAll('[name="cnf"]').length) {
                    submissions.push(self.flashHandler());
                }

                // Waiting for the requests to be completed
                Promise.all(submissions)
                .then(function ( data ) {
                    self.form.classList.remove('js-prevent-submit');
                    self.form.submit();
                })
                .catch(function(error){
                    self.afterSubmissionActions('error');
                });  
            }
        });
    }
}

// Events bound to fields
flashForm.prototype.fieldsEvents = function(){
    var self = this;

    // Filter Options Based on another field
    var options = self.form.querySelectorAll('option[data-filter]');
    if(options.length) {
        options.forEach(function() {
            var field = this;
            var filters = field.getAttribute('data-filter').split('|');
            var field = self.form.querySelector('[name="' + filters[0] + '"]');

            field.addEventListener('change', function(){
                if(field && (field.value == filters[1] || field.value == '' || !field.value)) {
                    field.style.display = '';
                } else {
                    field.style.display = 'none';
                }  
            });
        });
    }

    // Change settings based on a field
    var conditional_fields = self.form.querySelectorAll('[name="cnf"][data-conditional]')
    if(conditional_fields.length) {
        var listened_fields = [];
        conditional_fields.forEach(function(option){
            // Checking all the variants of the cnf
            if(listened_fields.indexOf(option.getAttribute('data-conditional')) == -1) {
                // Name of the field to watch for
                var field_name = option.getAttribute('data-conditional');
                var field = self.form.querySelector('[name="' + field_name + '"]');

                field.addEventListener('change', function(){
                    self.form.querySelectorAll('[name="cnf"]').forEach(function(field){
                        field.removeAttribute('checked');
                    });
                    
                    if(self.form.querySelector('[name="cnf"][data-conditional-value="' + field.value + '"]')) {
                        self.form.querySelector('[name="cnf"][data-conditional-value="' + field.value + '"]').setAttribute('checked', 'checked');
                    } else {
                        self.form.querySelector('[name="cnf"][data-conditional-default]').setAttribute('checked', 'checked');
                    }
                });

                listened_fields.push(option.getAttribute('data-conditional'));
            }
        });
    }
}

// Get form settings
flashForm.prototype.getSettings = function(){
    var self = this;
    
    // Initialise settings
    self.settings = {
        ajax_submit : true,
        enabled: true,
        prevent_thank_you_message: false
    };

    // Get the settings if they are there
    if(self.form.hasAttribute('data-flash-form')) {
        try {
            Object.assign(self.settings, JSON.parse('{' + self.form.getAttribute('data-flash-form') + '}'));
        } catch(error) {
            
        }
    }
}

// Pre submission actions
flashForm.prototype.preSubmissionActions = function(){
    var self = this;
    
    self.form.style.opacity = '0.6';

    // Custom Action
    if(self.settings.pre_submission_callback) {
        self.settings.enabled = eval(self.settings.pre_submission_callback);
    }
}

// After ajax submission events
flashForm.prototype.afterSubmissionActions = function(event){
    var self = this;

    switch (event) {
        case 'success':
            // Custom callback
            if(self.settings.success_callback) {
                eval(self.settings.success_callback);
            } else if(self.settings.thank_you_url) {
                // Redirect to Thank you Page
                Turbolinks.visit(self.settings.thank_you_url);
            }
            if(!self.settings.prevent_thank_you_message && !self.settings.thank_you_url) {               
                // Show Thank You message
                self.form.style.opacity = 1;
                self.form.querySelector('.form__container').style.display = 'none';
                self.form.querySelector('.form__error').style.display = 'none';
                if(self.form.querySelector('.form__thank_you_message')) {
                    flash.fadeIn(self.form.querySelector('.form__thank_you_message'));
                }
            } 
            break;
        
        case 'error':
            self.form.style.opacity = 1;
            flash.fadeIn(self.form.querySelector('.form__error'));
            break;
    }

    // Disable Submit button
    var submit_button = self.form.querySelector('[type="submit"]');
    if(submit_button) {
        submit_button.removeAttribute('disabled');
    }
}

// Submission on flash lambda handler
flashForm.prototype.flashHandler = async function(){
    var self = this;

    // Get all the file inputs and loads them into an s3 bucket, then it'll pass
    // the url of the files to the email handler that will download the files
    // and it'll attach them to the email
    var submit_files_requests = [];
    self.form.querySelectorAll('[type="file"]').forEach(function(file_input) {
        if(file_input.files && file_input.files.length) {
            for(var file of file_input.files) {
                console.log(file)
                // Getting signatures and sending files
                submit_files_requests.push(new Promise(function(resolve, reject){  
                    var request = new XMLHttpRequest();
                    request.open('POST', 'https://wvxig0amjb.execute-api.eu-west-1.amazonaws.com/production/forms-handler-uploads', true);
                    request.onload = function() {
                        if (this.status >= 200 && this.status < 400) {
                            // Got the signed url
                            var res_data = JSON.parse(request.responseText);
                            if(!res_data.url) {
                                resolve();
                            }
    
                            // Uploads file  
                            var upload_request = new XMLHttpRequest();
                            upload_request.open('PUT', res_data.url, true);
                            upload_request.setRequestHeader('Content-Type', file.type);
                            upload_request.setRequestHeader('Content-Encoding', 'UTF-8');
                            upload_request.onload = function() {
                                if (this.status >= 200 && this.status < 400) {
                                    var file_data = {};
                                    try {
                                        // If the file was uploaded, creates
                                        // an object to keep track of the field name
                                        // and the url of the uploaded file
                                        file_data = {
                                            field: 'AWS_attachment-' + file_input.name,
                                            name: file.name,
                                            url: res_data.url.split('?')[0]
                                        };
                                        file.value = '';
                                    } catch(e) {}
                                    resolve(file_data);
                                } else {
                                    reject();
                                }
                            }
                            upload_request.onerror = function(error) {
                                reject();
                            };
                            upload_request.send(file);
                        } else {
                            reject();
                        }
                    };
                    request.onerror = function(error) {
                        reject('no');
                    };
                    // Sending just the filename to upload the file
                    request.send('file=' + file.name);
                }));
            }
        }
    });

    // Waiting for all the files to be uploaded
    let submitted_files = await Promise.all(submit_files_requests);
    console.log(submitted_files)

    return (new Promise(function(resolve, reject){  
        var handler_settings = {
            url: self.flash_submissions_handler,
            type: 'POST',
            data: ''
        }

        if(self.form.getAttribute('enctype') == 'multipart/form-data') {
            handler_settings.data = new FormData(self.form);
            // Get all the files and removes the file inputs from the form
            // appending text inputs with the url of the files uploaded in s3
            if(submitted_files.length) {
                var files_to_send = {};
                // Getting the groups of files submitted with the same field 
                // (this is to support also multifile fields)
                submitted_files.forEach(function(submitted_file){
                    if(!files_to_send[submitted_file.field]) {
                        files_to_send[submitted_file.field] = [];
                    }
                    files_to_send[submitted_file.field].push(submitted_file.url);
                });
                // Replace fields
                Object.keys(files_to_send).forEach(function(field){
                    handler_settings.data.append(field, files_to_send[field].join(','));
                    handler_settings.data.delete(field.replace('AWS_attachment-', ''));
                });
                // Removing empty files
                self.form.querySelectorAll('[type="file"]').forEach(function(file_input) {
                    if(!file_input.files || !file_input.files.length) {
                        handler_settings.data.delete(file_input.name);
                    }
                });
            }
        } else {
            handler_settings.data = flash.serializeForm(self.form);
        }
        self.sendRequest(handler_settings, resolve, reject);
    }));
}

// Submission on custom handler
flashForm.prototype.otherHandler = function(){
    var self = this;

    return (new Promise(function(resolve, reject){  
        var handler_settings = {
            url: self.form.getAttribute('action'),
            type: "POST",
            data: ''
        };

        if(self.form.getAttribute('enctype') == 'multipart/form-data') {
            handler_settings.data = new FormData(self.form);
            handler_settings.cache = false;
            handler_settings.contentType = false;
            handler_settings.processData = false;
        } else {
            handler_settings.data = flash.serializeForm(self.form);
        }

        // Jsonp option in case of CORS not working with normal requests
        if(self.settings.enable_jsonp) {
            handler_settings.dataType = "jsonp";
            handler_settings.crossDomain = true;
        }

        self.sendRequest(handler_settings, resolve, reject);
    }));
}

flashForm.prototype.sendRequest = function(handler_settings, resolve, reject){
    var self = this;

    var request = new XMLHttpRequest();
    request.open('POST', handler_settings.url, true);

    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            resolve('yay');
        } else {
            reject('no');
        }
    };

    request.onerror = function(error) {
        reject('no');
    };
    
    request.send(handler_settings.data);
}

// Callback for jsonp requests
function formCallback(data) {
    if(data.submission_status) {
        self.afterSubmissionActions('success');
    } else {
        self.afterSubmissionActions('error');
    }
}

// Init function
function initFlashForms() {
    document.querySelectorAll('form:not(.js_form--no_flash):not(.js_form--just_in_lightbox), .js-lightbox form.js_form--just_in_lightbox').forEach(function(form){
        var flash_form = new flashForm(form);
    });
}

// Init
flash.ready(function(){
    initFlashForms();
});